import * as React from 'react'
import * as R from 'ramda'

import { List, ListItem } from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton'
import { loadEventRunners } from '../helpers/loaders'

export interface State {
  loading: boolean
  competitors: any
}
export interface Props {
  onClickBack: () => void
  currentEvent: string
}
export class Event extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      competitors: {}
    }
  }
  public async componentDidMount() {
    const eventData = await loadEventRunners(this.props.currentEvent)
    const competitors = eventData[this.props.currentEvent].competitors

    this.setState({
      loading: false,
      competitors
    })
  }

  public render(): JSX.Element {
    const listOfCompetitors = (
      <List style={styles.list}>
        {R.keys(this.state.competitors).map(key => {
          return (
            <ListItem
              key={key}
              style={styles.listItem}
              primaryText={this.state.competitors[key].Name}
              secondaryText={`position - ${this.state.competitors[key].Saddle}`}
            />
          )
        })}
      </List>
    )

    return (
      <div>
        <RaisedButton primary={true} onClick={this.props.onClickBack}>
          Back
        </RaisedButton>
        {this.state.loading && <div>Event Loading</div>}
        {!this.state.loading && listOfCompetitors}
      </div>
    )
  }
}

const styles: { [index: string]: React.CSSProperties } = {
  list: {
    maxWidth: '500px',
    width: '100%'
  },
  listItem: {
    borderBottom: '1px solid #ccc'
  }
}
export default Event
