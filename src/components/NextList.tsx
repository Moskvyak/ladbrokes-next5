import * as React from 'react'
import { List, ListItem } from 'material-ui/List'
import Subheader from 'material-ui/Subheader'
import CountDown from './CountDown'
import ActionInfo from 'material-ui/svg-icons/action/info'
import IconButton from 'material-ui/IconButton'
import { mapRaceType } from '../helpers/mapper'

export interface Props {
  onViewEventClick: (id: string) => void
  next5List: any[]
  removeExpiredEvent: (id: string) => void
}

export class NextList extends React.Component<Props, {}> {
  constructor(props) {
    super(props)
  }

  public render(): JSX.Element {
    const raceList = this.props.next5List || []
    return (
      <List style={styles.list}>
        <Subheader>Next 5 List</Subheader>
        {raceList.map(race => {
          const onRemoveExpiredEventFn = () => {
            this.props.removeExpiredEvent(race.EventID)
          }
          const raceType = mapRaceType(race.RaceType)
          const onViewEventClickFn = () => {
            this.props.onViewEventClick(race.EventID)
          }
          return (
            <ListItem
              style={styles.listItem}
              key={race.EventID}
              primaryText={`Event ID: ${race.EventID}, Race Type: ${raceType}`}
              secondaryText={
                <CountDown
                  suspendedTime={race.SuspendDateTime}
                  onTimeExpired={onRemoveExpiredEventFn}
                />
              }
              rightIconButton={
                <IconButton onClick={onViewEventClickFn}>
                  <ActionInfo />
                </IconButton>
              }
            />
          )
        })}
      </List>
    )
  }
}

const styles: { [index: string]: React.CSSProperties } = {
  list: {
    maxWidth: '500px',
    width: '100%'
  },
  listItem: {
    borderBottom: '1px solid #ccc'
  }
}
export default NextList
