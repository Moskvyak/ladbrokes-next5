import * as React from 'react'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import * as moment from 'moment-timezone'
export interface Props {
  suspendedTime: string
  onTimeExpired: () => void
}

@observer
class CountDown extends React.Component<Props, {}> {
  private interval
  @observable localSuspendedTime

  private countTimeLeft = (): string => {
    const currentTime = moment.tz('Australia/Brisbane')

    const suspendedTime = moment.tz(
      this.props.suspendedTime,
      'Australia/Sydney'
    )

    this.localSuspendedTime = suspendedTime
      .tz('Australia/Brisbane')
      .format('YYYYMMDD hh:mm:ss')

    const timeLeftInMinutes = suspendedTime.diff(currentTime, 'minutes')
    const timeLeftInSeconds =
      suspendedTime.diff(currentTime, 'seconds') - timeLeftInMinutes * 60

    const timeLeft = `${timeLeftInMinutes}m:${timeLeftInSeconds}s`

    if (suspendedTime.diff(currentTime) < 0) {
      this.props.onTimeExpired()
    }
    return timeLeft
  }

  @observable timeLeft = this.countTimeLeft()

  public componentWillMount() {
    this.interval = setInterval(() => {
      this.timeLeft = this.countTimeLeft()
    }, 1000)
  }

  public componentWillUnmount() {
    clearInterval(this.interval)
  }

  render() {
    return <div style={styles.countDown}>Starts in: {this.timeLeft}</div>
  }
}

const styles: { [index: string]: React.CSSProperties } = {
  countDown: {
    marginTop: '8px'
  }
}

export default CountDown
