import * as React from 'react'
import * as R from 'ramda'

import { loadRacingList } from './helpers/loaders'
import { ViewMode } from './interfaces'
import NextList from './components/NextList'
import Event from './components/Event'

export interface State {
  viewMode: ViewMode
  loading: boolean
  currentEvent: string | undefined
  currentList: any[]
}

export class App extends React.Component<any, State> {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      viewMode: 'list',
      currentEvent: undefined,
      currentList: []
    }
  }
  public async componentDidMount() {
    await this.reloadList()
  }

  private reloadList = async (): Promise<void> => {
    const response = await loadRacingList()
    const sortBySuspendedDateTime = R.sortBy(R.prop('SuspendDateTime'))
    let racingList = R.values(response).filter(x => x.Status === 'Open')
    racingList = sortBySuspendedDateTime(racingList)
    this.setState({
      loading: false,
      currentList: racingList
    })
  }

  private onViewEventClick = (id: string): void => {
    this.setState({
      viewMode: 'event',
      currentEvent: id
    })
  }
  private onClickBack = (): void => {
    this.setState({
      viewMode: 'list'
    })
  }
  private removeExpiredEvent = (id: string): void => {
    let racingList = this.state.currentList.filter(x => x.EventID !== id)
    this.setState(
      {
        currentList: racingList
      },
      () => this.reloadList()
    )
  }

  public render(): JSX.Element {
    const eventId = this.state.currentEvent || ''
    if (this.state.loading) {
      return <div> Loading...</div>
    }
    if (this.state.viewMode === 'list') {
      const next5List: any[] = R.take(5, this.state.currentList) || []
      return (
        <NextList
          onViewEventClick={this.onViewEventClick}
          next5List={next5List}
          removeExpiredEvent={this.removeExpiredEvent}
        />
      )
    } else {
      return <Event onClickBack={this.onClickBack} currentEvent={eventId} />
    }
  }
}
export default App
