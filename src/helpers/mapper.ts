const raceTypeMap = {
  G: 'Greyhounds',
  T: 'Thoroughbred',
  H: 'Harness'
}

export const mapRaceType = (key: string): string => {
  return raceTypeMap[key]
}
