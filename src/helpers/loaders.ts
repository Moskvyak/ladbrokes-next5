import config from '../config'

export const loadRacingList = async (): Promise<any> => {
  return new Promise((resolve, reject) =>
    fetch(`${config.apiUrl}/feed/racingList`, {
      method: 'GET'
    })
      .then(async result => {
        resolve(result.json())
      })
      .catch(reject)
  )
}

export const loadEventRunners = async (id: string): Promise<any> => {
  return new Promise((resolve, reject) =>
    fetch(`${config.apiUrl}/feed/eventRunners/${id}`, {
      method: 'GET'
    })
      .then(async result => {
        resolve(result.json())
      })
      .catch(reject)
  )
}
