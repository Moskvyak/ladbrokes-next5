var express = require('express')
var path = require('path')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var rp = require('request-promise')
var http = require('http')
var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  )
  next()
})

app.use('/api/feed/racingList', async function(req, res, next) {
  var options = {
    method: 'GET',
    uri: `https://ladbrokes.com.au/api/feed/racingList`,
    json: true
  }
  rp(options)
    .then(response => {
      console.log(response)
      res.json(response)
      res.status(200)
    })
    .catch(error => {
      console.log(error)
      if (error.statusCode == 500) {
        res.json({ error: error.error })
      } else {
        res.json({ error: { status: 500, message: 'Unknown error' } })
      }

      res.status(500)
    })
})
app.use('/api/feed/eventRunners/:id', async function(req, res, next) {
  var options = {
    method: 'GET',
    uri: `https://ladbrokes.com.au/api/feed/eventRunners?event_id=${
      req.params.id
    }`,
    json: true
  }
  rp(options)
    .then(response => {
      console.log(response)
      res.json(response)
      res.status(200)
    })
    .catch(error => {
      console.log(error)
      if (error.statusCode == 500) {
        res.json({ error: error.error })
      } else {
        res.json({ error: { status: 500, message: 'Unknown error' } })
      }

      res.status(500)
    })
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

var port = normalizePort(process.env.PORT || '3001')
app.set('port', port)

/**
 * Create HTTP server.
 */

var server = http.createServer(app)

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port)
server.on('error', onError)
server.on('listening', onListening)

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address()
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  console.log('Listening on ' + bind)
}

module.exports = app
